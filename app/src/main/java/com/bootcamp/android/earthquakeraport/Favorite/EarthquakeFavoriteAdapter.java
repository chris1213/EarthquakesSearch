package com.bootcamp.android.earthquakeraport.Favorite;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bootcamp.android.earthquakeraport.R;
import com.bootcamp.android.earthquakeraport.Database.EarthquakeContract;

public class EarthquakeFavoriteAdapter extends CursorAdapter {


    private String favoriteLocation;
    private String favoriteMagnitude;
    private String favoriteDate;


    public EarthquakeFavoriteAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.earthquakes_favorites_adapter, viewGroup, false);
    }


    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {


        int locationColumnIndex = cursor.getColumnIndex(EarthquakeContract.EarthquakeEntry.COLUMN_EARTHQUAKE_LOCATION);
        int magnitudeColumnIndex = cursor.getColumnIndex(EarthquakeContract.EarthquakeEntry.COLUMN_EARTHQUAKE_MAGNITUDE);
        int dateColumnIndex = cursor.getColumnIndex(EarthquakeContract.EarthquakeEntry.COLUMN_EARTHQUAKE_DATE);

        favoriteLocation = cursor.getString(locationColumnIndex);
        favoriteMagnitude = cursor.getString(magnitudeColumnIndex);
        favoriteDate = cursor.getString(dateColumnIndex);


        TextView locationFavorite = (TextView) view.findViewById(R.id.location_favorite);
        locationFavorite.setText(favoriteLocation);

        TextView magnetudeFavorite = (TextView) view.findViewById(R.id.magnetude_favorite);
        magnetudeFavorite.setText(favoriteMagnitude);

        TextView timeFavorite = (TextView) view.findViewById(R.id.time_favorite);
        timeFavorite.setText(favoriteDate);

        ImageView deleteItemFavorite = (ImageView)view.findViewById(R.id.delete_item_favorite);
        deleteItemFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                long currentId = getItemId(cursor.getPosition());
                Uri currentUri = ContentUris.withAppendedId(EarthquakeContract.EarthquakeEntry.CONTENT_URI, currentId);
                context.getContentResolver().delete(currentUri, null, null);

            }
        });
    }
}
