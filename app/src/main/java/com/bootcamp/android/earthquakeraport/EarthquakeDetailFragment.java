package com.bootcamp.android.earthquakeraport;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bootcamp.android.earthquakeraport.Database.EarthquakeContract;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EarthquakeDetailFragment extends Fragment {

    private static final int EARTHQUAKE_LOADER = 1;

    private String location;
    private String dateString;
    private String magnitude;
    private Earthquake earthquakeItem;

    private String locationOffset = "";
    private String locationPrimary = "";

    private ImageView star;

    public EarthquakeDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        earthquakeItem = (Earthquake) getArguments().getSerializable("KEY");

        location = earthquakeItem.getLocation();
        if (location.contains("of")) {
            String[] parts = location.split("of");
            locationOffset = parts[0] + "of";
            locationPrimary = parts[1];
        } else {
            locationOffset = "";
            locationPrimary = location;
        }

        long dateMilli = earthquakeItem.getTimeInMilliseconds();
        Date date = new Date(dateMilli);

        double magnetudeOrig = earthquakeItem.getMagnitude();

        magnitude = formatMagnitude(magnetudeOrig);
        dateString = formatDate(date);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.earthquake_detail, container, false);

        ((TextView) rootView.findViewById(R.id.location_tv)).setText(locationPrimary);
        ((TextView) rootView.findViewById(R.id.magnetude_tv)).setText(String.valueOf(magnitude));
        ((TextView) rootView.findViewById(R.id.time_tv)).setText(dateString);

        star = (ImageView)rootView.findViewById(R.id.star_add_to_favorite);
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToFavorite();
            }
        });

        return rootView;
    }

    private void addToFavorite() {
        String earthquakeLocation = locationPrimary;
        String earthquakeMagnitude = magnitude;
        String earthquakeDate = dateString;

        ContentValues values = new ContentValues();
        values.put(EarthquakeContract.EarthquakeEntry.COLUMN_EARTHQUAKE_LOCATION, earthquakeLocation);
        values.put(EarthquakeContract.EarthquakeEntry.COLUMN_EARTHQUAKE_MAGNITUDE, earthquakeMagnitude);
        values.put(EarthquakeContract.EarthquakeEntry.COLUMN_EARTHQUAKE_DATE, earthquakeDate);

        Uri newUri = getContext().getContentResolver().insert(EarthquakeContract.EarthquakeEntry.CONTENT_URI, values);
        if (newUri == null) {
            Toast.makeText(getContext(), "error: insert position",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "location added",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private String formatDate(Date dateObject) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");
        return dateFormat.format(dateObject);
    }

    private String formatMagnitude(double magnitude) {
        DecimalFormat magnitudeFormat = new DecimalFormat("0.0");
        return magnitudeFormat.format(magnitude);
    }
}
